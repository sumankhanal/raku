## [Unreleased]




### 2021-11-18

- Baseline working state !
- Added snapshot
- Keybindings
- Plenty of snippets
- Highlights comments properly
- Uncomment blockComment in `language-configuration.json`