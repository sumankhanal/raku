# Raku extension for VS Code

## Features

- Syntax highlighting
- Plenty of snippets
- Keybindings

## Keybindings

| Keys           | Binding symbol |
| :------------- | -------------: |
| `Alt+Right`    |              » |
| `Alt+Left`     |              « |
| `Ctrl+Shift+M` |             => |



# Snippets

| Snippet |                                  Expansion |
| :------ | -----------------------------------------: |
| shb     |                        #!/usr/bin/env raku |
| slcom   |                        single line comment |
| emcom   |                           embedded comment |
| mlcom   |                          multiline comment |
| podcom  |                                pod comment |
| scal    |                                     scalar |
| arr     |                                      array |
| hash    |                                       hash |
| kv      |                         write key-val pair |
| for     |                                   for loop |
| forwhen |                         for loop with when |
| forp    |                 for loop with pointy block |
| if      |                             if conditional |
| elsif   |                               elsif branch |
| else    |                      concluding else block |
| while   |                                while block |
| sub     |                            define function |
| cls     |                           class definition |
| md      |                        method inside class |
| multi   |                          multi subroutines |
| given   |                       given when statement |
| loop    |                             loop statement |
| gm      |                           creating grammar |
| token   |                                      token |
| role    |                               role grammar |
| rule    |                                       rule |
| regex   |                               define regex |
| main    |                              MAIN function |
| natc1   |                             use NativeCall |
| natc2   |                argless nativecall function |
| natc3   | name of the native routine in your library |



# Snapshots

<img src="images/raku_extension.gif" width="800" height="400">